Feature: Trigger Patch_DataDriven_API

Scenario Outline: Trigger the patch API request with valid request parameters
		Given Enter "<Name>" and "<Job>" in patch request body
		When Send the patch request with data
		Then Validate data_driven_patch status code
		And Validate data_driven_patch response body parameters

Examples: 
		|Name |Job |
		|Arun|QA|
		|Anil|SrQA|
		|Anuraj|Dev|
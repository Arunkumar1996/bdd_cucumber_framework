package StepDefinitions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import Endpoint.Patch_Endpoint;
import Test_package.put_TC1;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import requestRepository.Patch_Request_Repository;

public class PatchStepDefintion extends Common_method_handle_API{
	File log_dir;
	String requestbody;
	String endpoint;
	int statuscode;
	String responsebody;
	
	@Given("Enter NAME and JOB in Patchrequest body")
	public void enter_name_and_job_in_patchrequest_body() throws IOException {
		log_dir = Handle_directory.create_log_directory("patch_TC1_logs");
		 requestbody=Patch_Request_Repository.Patch_request_TC1();
		 endpoint=Patch_Endpoint.Patch_Endpoint_Tc1();
	   // throw new io.cucumber.java.PendingException();
	}
	@When("Send the Patch request with payload")
	public void send_the_patch_request_with_payload() {
		statuscode = Common_method_handle_API.Patch_statuscode(requestbody, endpoint);
		responsebody = Common_method_handle_API.Patch_responseBody(requestbody, endpoint);
		System.out.println(responsebody);
	   // throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Patch status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(statuscode, 200);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate Patch response body parameters")
	public void validate_patch_response_body_parameters() throws IOException {
		Handle_api_logs.evidence_creator(log_dir, "patch_TC1", endpoint, requestbody, responsebody);
		put_TC1.validator(requestbody, responsebody);
		System.out.println("PatchAPI Response Validation Successfull");
	  //  throw new io.cucumber.java.PendingException();

}
}

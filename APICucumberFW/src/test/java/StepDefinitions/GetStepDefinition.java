package StepDefinitions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import Endpoint.Get_Endpoint;
import Test_package.get_TC1;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GetStepDefinition extends Common_method_handle_API {
	File log_dir;
	String endpoint;
	int statuscode;
	String responsebody;

	@Given("Enter GetEndpoint")
	public void enter_get_endpoint() {
		 log_dir = Handle_directory.create_log_directory("get_TC1_logs");
		 endpoint=Get_Endpoint.Get_Endpoint_Tc1();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Get API request")
	public void send_the_get_api_request() {
		statuscode = Common_method_handle_API.get_statuscode(endpoint);
		responsebody = Common_method_handle_API.get_responseBody(endpoint);
		System.out.println(responsebody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Get status code")
	public void validate_get_status_code() {
		Assert.assertEquals(statuscode, 200);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Get response body parameters")
	public void validate_get_response_body_parameters() throws IOException {
	get_TC1.validator(responsebody);
	Handle_api_logs.evidence_creator(log_dir,"get_TC1", endpoint,  responsebody);
	System.out.println("GetAPI Response Validation Successfull");



		// throw new io.cucumber.java.PendingException();
	}

}

package StepDefinitions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import Endpoint.Put_Endpoint;
import Test_package.put_TC1;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import requestRepository.Put_Request_Repository;

public class PutStepDefintion extends Common_method_handle_API {
	
		File log_dir;
		String requestbody;
		String endpoint;
		int statuscode;
		String responsebody;

@Given("Enter NAME and JOB in Putrequest body")
public void enter_name_and_job_in_putrequest_body() throws IOException {
	log_dir = Handle_directory.create_log_directory("put_TC1_logs");
	 requestbody=Put_Request_Repository.Put_Request_Tc1();
	endpoint=Put_Endpoint.Put_Enpoint_Tc1();
    //throw new io.cucumber.java.PendingException();
}
@When("Send the Put request with payload")
public void send_the_put_request_with_payload() {
	statuscode = Common_method_handle_API.Put_statuscode(requestbody, endpoint);
	responsebody = Common_method_handle_API.Put_responseBody(requestbody, endpoint);
	System.out.println(responsebody);
   
    //throw new io.cucumber.java.PendingException();
}
@Then("Validate Put status code")
public void validate_put_status_code() {
	Assert.assertEquals(statuscode, 200);
    //throw new io.cucumber.java.PendingException();
}
@Then("Validate Put response body parameters")
public void validate_put_response_body_parameters() throws IOException {
	Handle_api_logs.evidence_creator(log_dir, "put_TC1", endpoint, requestbody, responsebody);
	put_TC1.validator(requestbody, responsebody);
	System.out.println("PutAPI Response Validation Successfull");
   // throw new io.cucumber.java.PendingException();
}


}

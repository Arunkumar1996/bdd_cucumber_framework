package StepDefinitions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import Endpoint.Del_Endpoint;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DeleteStepDefintion extends Common_method_handle_API {
	File log_dir;
	String endpoint;
	int statuscode;

	@Given("Enter Delete Endpoint")
	public void enter_delete_endpoint() {
		 log_dir = Handle_directory.create_log_directory("del_TC1_logs");
		 endpoint=Del_Endpoint.Del_Endpoint_tc1();
		//throw new io.cucumber.java.PendingException();
	}

	@When("Send the Delete API request")
	public void send_the_delete_api_request() {
		statuscode=Common_method_handle_API.del_statuscode(endpoint);
		//throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Delete status code")
	public void validate_delete_status_code() throws IOException {
		Assert.assertEquals(statuscode, 204);
		Handle_api_logs.evidence_creator(log_dir,"del_TC1", endpoint, endpoint);
		System.out.println("Delete API Validation Successfull");

		//throw new io.cucumber.java.PendingException();

	}
}

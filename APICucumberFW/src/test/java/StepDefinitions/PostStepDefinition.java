package StepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import requestRepository.Post_Request_Repository;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import Endpoint.Post_Endpoint;
import Test_package.post_TC1;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;;

public class PostStepDefinition extends Common_method_handle_API {
	File log_dir;
	String requestbody;
	String endpoint;
	int statuscode;
	String responsebody;

	@Given("Enter NAME and JOB in Postrequest body")
	public void enter_name_and_job_in_request_body()throws IOException {
		log_dir = Handle_directory.create_log_directory("post_TC1_logs");
		endpoint = Post_Endpoint.Post_Endpoint_Tc1();
		requestbody = Post_Request_Repository.Post_request_Tc1();
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the Post request with payload")
	public void send_the_request_with_payload() {
		statuscode = Common_method_handle_API.Post_statuscode(requestbody, endpoint);
		responsebody = Common_method_handle_API.Post_responseBody(requestbody, endpoint);
		System.out.println(responsebody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Post status code")
	public void validate_status_code() {
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate Post response body parameters")
	public void validate_response_body_parameters() throws IOException {
		Handle_api_logs.evidence_creator(log_dir, "post_TC1", endpoint, requestbody, responsebody);
		post_TC1.validator(requestbody, responsebody);
		System.out.println("PostAPI Response Validation Successfull");
		// throw new io.cucumber.java.PendingException();
	}

}

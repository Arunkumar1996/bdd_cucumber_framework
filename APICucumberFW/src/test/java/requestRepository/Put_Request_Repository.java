package requestRepository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_Common_method.Data_Extractor_Excel;

public class Put_Request_Repository {
	public static String Put_Request_Tc1() throws IOException {
		ArrayList<String> Data = Data_Extractor_Excel.Data_Extractor_reader("Test_data", "put_api", "put_TC2");
		String name = Data.get(1);
		String job = Data.get(2);
		String Put_requestbody ="{\r\n"+ "    \"name\": \""+name+"\",\r\n"+ "    \"job\": \""+job+"\"\r\n"+ "}"; 
		return Put_requestbody;
	}

}

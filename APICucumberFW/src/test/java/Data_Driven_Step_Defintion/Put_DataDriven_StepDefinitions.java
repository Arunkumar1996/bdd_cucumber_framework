package Data_Driven_Step_Defintion;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import Endpoint.Put_Endpoint;
import Test_package.put_TC1;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Put_DataDriven_StepDefinitions {
	File log_dir;
	String requestbody;
	String endpoint;
	int statuscode;
	String responsebody;

	@Given("Enter {string} and {string} in put request body")
	public void enter_and_in_put_request_body(String req_name, String req_job) {
		log_dir = Handle_directory.create_log_directory("put_TC1_logs");
		endpoint = Put_Endpoint.Put_Enpoint_Tc1();
		requestbody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the put request with data")
	public void send_the_put_request_with_data() {
		statuscode = Common_method_handle_API.Put_statuscode(requestbody, endpoint);
		responsebody = Common_method_handle_API.Put_responseBody(requestbody, endpoint);
		System.out.println(responsebody);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate data_driven_put status code")
	public void validate_data_driven_put_status_code() {
		Assert.assertEquals(statuscode, 200);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate data_driven_put response body parameters")
	public void validate_data_driven_put_response_body_parameters() throws IOException {
		Handle_api_logs.evidence_creator(log_dir, "put_TC1", endpoint, requestbody, responsebody);
		put_TC1.validator(requestbody, responsebody);
		System.out.println("PutAPI_Data_Driven Response Validation Successfull");
		// throw new io.cucumber.java.PendingException();
	}

}

package Data_Driven_Step_Defintion;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import Endpoint.Patch_Endpoint;
import Test_package.put_TC1;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Patch_data_driven_StepDefinitions {
	File log_dir;
	String requestbody;
	String endpoint;
	int statuscode;
	String responsebody;

	@Given("Enter {string} and {string} in patch request body")
	public void enter_and_in_patch_request_body(String req_name, String req_job) {
		log_dir = Handle_directory.create_log_directory("patch_TC1_logs");
		 endpoint=Patch_Endpoint.Patch_Endpoint_Tc1();
		 requestbody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
					+ "}";
	    //throw new io.cucumber.java.PendingException();
	}
	@When("Send the patch request with data")
	public void send_the_patch_request_with_data() {
		statuscode = Common_method_handle_API.Patch_statuscode(requestbody, endpoint);
		responsebody = Common_method_handle_API.Patch_responseBody(requestbody, endpoint);
		System.out.println(responsebody);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_patch status code")
	public void validate_data_driven_patch_status_code() {
		Assert.assertEquals(statuscode, 200);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_patch response body parameters")
	public void validate_data_driven_patch_response_body_parameters() throws IOException {
		Handle_api_logs.evidence_creator(log_dir, "patch_TC1", endpoint, requestbody, responsebody);
		put_TC1.validator(requestbody, responsebody);
		System.out.println("PatchAPI_Data_Driven Response Validation Successfull");
	    //throw new io.cucumber.java.PendingException();
	}




}

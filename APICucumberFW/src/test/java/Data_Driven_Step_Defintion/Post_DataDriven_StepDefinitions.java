package Data_Driven_Step_Defintion;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_common_methods.Common_method_handle_API;
import Endpoint.Post_Endpoint;
import Test_package.post_TC1;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import requestRepository.Post_Request_Repository;

public class Post_DataDriven_StepDefinitions {
	File log_dir;
	String endpoint;
	String requestBody;
	int statuscode;
	String responseBody;
	@Given("Enter {string} and {string} in post request body")
	public void enter_and_in_post_request_body(String req_name, String req_job) throws IOException {
	log_dir = Handle_directory.create_log_directory("post_TC1_logs");
	endpoint = Post_Endpoint.Post_Endpoint_Tc1();
	requestBody ="{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
	}
	@When("Send the post request with data")
	public void send_the_post_request_with_data() {
		statuscode = Common_method_handle_API.Post_statuscode(requestBody, endpoint);
		responseBody = Common_method_handle_API.Post_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_post status code")
	public void validate_data_driven_post_status_code() {
		Assert.assertEquals(statuscode, 201);		
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate data_driven_post response body parameters")
	public void validate_data_driven_post_response_body_parameters() throws IOException {
		Handle_api_logs.evidence_creator(log_dir, "post_TC1", endpoint, requestBody, responseBody);
		post_TC1.validator(requestBody, responseBody);
		System.out.println("PostAPI_Datadriven Response Validation Successfull");
	   // throw new io.cucumber.java.PendingException();
	}
}

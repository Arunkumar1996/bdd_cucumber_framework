Feature: Trigger GetAPI 
@Get_API_TestCases
Scenario: Trigger the  GetAPI with valid the parameter 
	Given Enter GetEndpoint
	When Send the Get API request 
	Then Validate Get status code 
	And Validate Get response body parameters 

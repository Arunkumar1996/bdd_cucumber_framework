Feature: Trigger PutAPI 
@Put_API_TestCases
Scenario: Trigger the  PutAPI request with valid request parameters 
	Given Enter NAME and JOB in Putrequest body 
	When Send the Put request with payload 
	Then Validate Put status code 
	And Validate Put response body parameters 
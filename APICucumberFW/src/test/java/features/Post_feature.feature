Feature: Trigger PostAPI 
@Post_API_TestCases
Scenario: Trigger the  PostAPI request with valid request parameters 
	Given Enter NAME and JOB in Postrequest body 
	When Send the Post request with payload 
	Then Validate Post status code 
	And Validate Post response body parameters 
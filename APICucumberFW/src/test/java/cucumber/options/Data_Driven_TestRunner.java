package cucumber.options;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith (Cucumber.class)
@CucumberOptions(features = "src/test/java/Data_Driven_Features",glue ="Data_Driven_Step_Defintion")
public class Data_Driven_TestRunner {

}

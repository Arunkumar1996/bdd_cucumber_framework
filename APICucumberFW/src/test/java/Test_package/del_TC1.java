package Test_package;

import java.io.File;

import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoint.Del_Endpoint;
import Utility_Common_method.Handle_directory;


public class del_TC1 extends Common_method_handle_API {
	 static File log_dir;
	 String endpoint;
	int statuscode;
	@Test
	public static void delete_executor() {
		//String del_endpoint = "https://reqres.in/api/users/2";
		 log_dir = Handle_directory.create_log_directory("del_TC1_logs");
		String del_endpoint=Del_Endpoint.Del_Endpoint_tc1();
		for (int i = 0; i < 5; i++) {
			int del_statusCode = del_statuscode(del_endpoint);
			if (del_statusCode == 204) {
				System.out.println(del_statusCode);
				break;
			} else {
				System.out.println("expected statuscode is not found hence retrying");
			}

		}
	}

}
